package com.canal.android.test.domain.model

class Program(
    val id: String,
    val title: String,
    val subtitle: String,
    val urlImage: String,
    val urlLogoChannel: String?,
    val navigateTo: NavigateTo
)
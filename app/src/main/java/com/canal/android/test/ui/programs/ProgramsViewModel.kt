package com.canal.android.test.ui.programs

import androidx.lifecycle.LiveData
import com.canal.android.test.common.error.isNetworkRelated
import com.canal.android.test.domain.usecase.GetProgramsUseCase
import com.canal.android.test.ui.common.BaseViewModel
import com.canal.android.test.ui.common.livedata.SingleLiveEvent
import com.canal.android.test.ui.programs.mapper.ProgramsUiMapper
import com.canal.android.test.ui.programs.model.PageProgramsUi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class ProgramsViewModel(
    private val getProgramsUseCase: GetProgramsUseCase,
    private val programsUiMapper: ProgramsUiMapper
) : BaseViewModel<PageProgramsUi>() {

    private val disposables = CompositeDisposable()
    private val _event = SingleLiveEvent<Event>()

    val event: LiveData<Event> get() = _event

    init {
        getPrograms()
    }

    fun onRetryClicked() {
        getPrograms()
    }

    private fun getPrograms() {
        disposables.add(getProgramsUseCase()
            .map { programs ->
                programsUiMapper.toUi(programs)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::postUiData, ::onErrorReceived))
    }

    private fun onErrorReceived(e: Throwable) {
        _event.value = when {
            e.isNetworkRelated() -> Event.Error.NetworkError
            else -> Event.Error.GenericError(e.message)
        }
    }


    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    sealed class Event {
        sealed class Error : Event() {
            object NetworkError : Error()
            class GenericError(val message: String?) : Error()
        }
        // there could be other non error events also
    }
}
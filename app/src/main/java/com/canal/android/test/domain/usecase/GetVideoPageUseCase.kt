package com.canal.android.test.domain.usecase

import com.canal.android.test.domain.Repository
import com.canal.android.test.domain.model.VideoPage
import io.reactivex.Single

class GetVideoPageUseCase (
    private val repository: Repository
) {

    operator fun invoke(url: String): Single<VideoPage> = repository.getVideoPage(url)
}
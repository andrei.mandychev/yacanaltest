package com.canal.android.test.repository.mapper

import com.canal.android.test.domain.model.DetailsPage
import com.canal.android.test.repository.api.model.DetailsPageApi
import com.canal.android.test.repository.mapper.DetailsPageMapper.ProtocolException.*
import java.lang.Exception

class DetailsPageMapper : BaseDomainMapper<DetailsPageApi, DetailsPage>() {

    override fun toDomain(api: DetailsPageApi): DetailsPage {
        return api.detail?.let { detail ->
            detail.informations?.let { info ->

                DetailsPage(
                    info.id ?: throw NullIdException,
                    info.title ?: throw NullTitleException,
                    info.subtitle ?: throw NullSubtitleException,
                    info.urlImage ?: throw NullImageUrlException,
                    info.summary ?: throw NullSummaryException,
                    info.editorialTitle ?: throw NullEditorialTitleException,
                    info.urlMedia ?: throw NullMediaUrlException
                )
            } ?: throw NullInformationException
        } ?: throw NullDetailException
    }

    sealed class ProtocolException : Exception() {
        object NullDetailException : ProtocolException()
        object NullInformationException : ProtocolException()
        object NullIdException : ProtocolException()
        object NullTitleException : ProtocolException()
        object NullSubtitleException : ProtocolException()
        object NullMediaUrlException : ProtocolException()
        object NullSummaryException : ProtocolException()
        object NullEditorialTitleException : ProtocolException()
        object NullImageUrlException : ProtocolException()
    }
}
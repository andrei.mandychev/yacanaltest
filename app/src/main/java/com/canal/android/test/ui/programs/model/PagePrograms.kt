package com.canal.android.test.ui.programs.model

import android.os.Parcelable
import com.canal.android.test.domain.model.NavigateTo
import kotlinx.android.parcel.Parcelize

data class PageProgramsUi(
    val programs: List<ProgramUi>
)

@Parcelize
data class ProgramUi(
    val id: String,
    val title: String,
    val subtitle: String,
    val urlImage: String,
    val urlLogoChannel: String?,
    val navigateTo: NavigateTo
) : Parcelable
package com.canal.android.test.ui.details

import androidx.lifecycle.LiveData
import com.canal.android.test.common.error.isNetworkRelated
import com.canal.android.test.domain.model.NavigateTo
import com.canal.android.test.domain.usecase.GetDetailsPageUseCase
import com.canal.android.test.repository.mapper.DetailsPageMapper
import com.canal.android.test.ui.common.BaseViewModel
import com.canal.android.test.ui.common.livedata.SingleLiveEvent
import com.canal.android.test.ui.details.mapper.DetailsUiMapper
import com.canal.android.test.ui.details.model.DetailsUi
import com.canal.android.test.ui.programs.model.ProgramUi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class DetailsViewModel(
    private val programUi: ProgramUi,
    private val getDetailsPageUseCase: GetDetailsPageUseCase,
    private val mapper: DetailsUiMapper
) : BaseViewModel<DetailsUi>() {

    private lateinit var detailsUi: DetailsUi
    private val disposables = CompositeDisposable()
    private val _event = SingleLiveEvent<Event>()

    val event: LiveData<Event> get() = _event

    init {
        getDetailsPage()
    }

    fun onPlayClicked() {
        _event.value = Event.OnPlayClicked(detailsUi.urlMedias)
    }

    fun onRetryClicked() {
        getDetailsPage()
    }

    private fun getDetailsPage() {
        val url = (programUi.navigateTo as NavigateTo.DetailPage).urlPage
        disposables.add(getDetailsPageUseCase(url)
            .map { mapper.toUi(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::onDetailsUiReceived, ::onErrorReceived)
        )
    }

    private fun onDetailsUiReceived(data: DetailsUi) {
        detailsUi = data
        postUiData(data)
    }

    private fun onErrorReceived(e: Throwable) {
        _event.value = when {
            e.isNetworkRelated() -> Event.Error.NetworkError
            e is DetailsPageMapper.ProtocolException -> Event.Error.ProtocolError
            else -> Event.Error.GenericError(e.message)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    sealed class Event {
        class OnPlayClicked(val videoUrl: String) : Event()
        sealed class Error : Event() {
            object NetworkError : Error()
            object ProtocolError : Error()
            class GenericError(val message: String?) : Error()
        }
    }
}
package com.canal.android.test.ui.player

import android.content.Context
import android.net.Uri
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF
import com.google.android.exoplayer2.RenderersFactory
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.FileDataSource
import com.google.android.exoplayer2.upstream.cache.*
import com.google.android.exoplayer2.util.Util
import java.io.File

class PlayerHelper(private val context: Context) {

    private val userAgent by lazy { Util.getUserAgent(context, "ExoPlayerDemo") }

    private val httpDataSourceFactory by lazy { DefaultHttpDataSourceFactory(userAgent) }

    private val downloadDirectory by lazy { context.getExternalFilesDir(null) ?: context.filesDir }

    private val databaseProvider by lazy { ExoDatabaseProvider(context) }

    private val downloadContentDirectory by lazy { File(downloadDirectory, DOWNLOAD_CONTENT_DIRECTORY) }

    private val downloadCache: Cache by lazy {
        SimpleCache(downloadContentDirectory, NoOpCacheEvictor(), databaseProvider)
    }

    private val dataSourceFactory: DataSource.Factory by lazy {
        buildDataSourceFactory()
    }

    val rendersFactory: RenderersFactory by lazy {
        DefaultRenderersFactory(context).setExtensionRendererMode(EXTENSION_RENDERER_MODE_OFF)
    }

    fun createMediaSource(url: String): MediaSource =
        HlsMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(url))

    private fun buildDataSourceFactory(): DataSource.Factory {
        val upstreamFactory =
            DefaultDataSourceFactory(context, httpDataSourceFactory)
        return buildReadOnlyCacheDataSource(
            upstreamFactory,
            downloadCache
        )
    }

    private fun buildReadOnlyCacheDataSource(upstreamFactory: DataSource.Factory, cache: Cache) =
        CacheDataSourceFactory(
            cache,
            upstreamFactory,
            FileDataSource.Factory(),  /* cacheWriteDataSinkFactory= */
            null,
            CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,  /* eventListener= */
            null
        )

    private companion object {
        const val DOWNLOAD_CONTENT_DIRECTORY = "downloads"
    }
}
package com.canal.android.test.ui.programs.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.canal.android.test.R
import com.canal.android.test.ui.programs.model.ProgramUi
import com.canal.android.test.ui.programs.view.ProgramViewHolder

class ProgramsAdapter(
    private val clickListener: (ProgramUi) -> Unit
) : ListAdapter<ProgramUi, ProgramViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgramViewHolder {

        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.layout_program, parent, false)
        return ProgramViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProgramViewHolder, position: Int) {
        val programUi = getItem(position)
        holder.bind(programUi)
        holder.containerView.setOnClickListener { clickListener(programUi) }
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<ProgramUi>() {
            override fun areItemsTheSame(
                oldItem: ProgramUi,
                newItem: ProgramUi
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ProgramUi,
                newItem: ProgramUi
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
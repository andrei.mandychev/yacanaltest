package com.canal.android.test.domain.di

import com.canal.android.test.domain.usecase.GetDetailsPageUseCase
import com.canal.android.test.domain.usecase.GetVideoPageUseCase
import com.canal.android.test.domain.usecase.GetProgramsUseCase
import org.koin.dsl.module

val domainUseCaseModule = module {
    factory {
        GetProgramsUseCase(
            repository = get()
        )
    }
    factory {
        GetVideoPageUseCase(
            repository = get()
        )
    }
    factory {
        GetDetailsPageUseCase(
            repository = get()
        )
    }
}

val koinDomainModules = listOf(
    domainUseCaseModule
)
package com.canal.android.test.repository.api.model

import com.squareup.moshi.Json

data class VideoPageApi(
    val detail: DetailApi?
) {

    data class DetailApi(
        val informations: InformationsApi?
     ) {

        data class InformationsApi(
            @Json(name = "videoURLs") val videoUrls: List<VideoUrlApi>?
        ) {

            data class VideoUrlApi(
                @Json(name = "videoURL") val url: String?,
                val encryption: String?
            )
        }
    }

}
package com.canal.android.test.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.canal.android.test.R
import com.canal.android.test.ui.details.model.DetailsUi
import com.canal.android.test.ui.programs.model.ProgramUi
import com.canal.android.test.ui.details.DetailsViewModel.Event
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class DetailsFragment : Fragment() {

    private val viewModel: DetailsViewModel by viewModel {
        parametersOf(programUi)
    }

    private lateinit var programUi: ProgramUi

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        programUi = DetailsFragmentArgs.fromBundle(requireNotNull(arguments)).programUi
        initViews()
        viewModel.uiData.observe(viewLifecycleOwner, Observer { onDetailsUiReceived(it) })
        viewModel.event.observe(viewLifecycleOwner, Observer { onEventReceived(it) })
    }

    private fun initViews() {
        Glide.with(this)
            .load(programUi.urlImage)
            .fitCenter()
            .into(details_image)

        details_title.text = programUi.title
        details_subtitle.text = programUi.subtitle
        details_button_play.setOnClickListener { viewModel.onPlayClicked() }
    }

    private fun onDetailsUiReceived(detailsUi: DetailsUi) {
        details_button_play.isVisible = true
        details_summary.isVisible = true
        details_editorial_title.isVisible = true

        details_editorial_title.text = detailsUi.editorialTitle
        details_summary.text = detailsUi.summary
    }

    private fun onEventReceived(event: Event) {
        when (event) {
            is Event.OnPlayClicked -> {
                val direction = DetailsFragmentDirections.actionDetailsFragmentToPlayerFragment(event.videoUrl)
                findNavController().navigate(direction)
            }
            is Event.Error -> showErrorEvent(event)
        }
    }

    private fun showErrorEvent(errorEvent: Event.Error) {
        val errorMessage = when (errorEvent) {
            is Event.Error.GenericError -> errorEvent.message
            is Event.Error.NetworkError -> getString(R.string.error_network)
            is Event.Error.ProtocolError -> getString(R.string.error_protocol)
        } ?: getString(R.string.error_generic)

        Snackbar.make(details_content, errorMessage, Snackbar.LENGTH_LONG)
            .setAction(R.string.retry) { viewModel.onRetryClicked() }
            .show()
    }
}
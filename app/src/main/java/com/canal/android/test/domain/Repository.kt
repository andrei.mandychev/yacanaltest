package com.canal.android.test.domain

import com.canal.android.test.domain.model.DetailsPage
import com.canal.android.test.domain.model.Program
import com.canal.android.test.domain.model.VideoPage
import io.reactivex.Single

interface Repository {

    fun getPrograms(url: String): Single<List<Program>>

    fun getVideoPage(url: String): Single<VideoPage>

    fun getDetailsPage(url: String): Single<DetailsPage>
}
package com.canal.android.test.ui.player

import androidx.lifecycle.LiveData
import com.canal.android.test.common.error.isNetworkRelated
import com.canal.android.test.domain.usecase.GetVideoPageUseCase
import com.canal.android.test.ui.common.BaseViewModel
import com.canal.android.test.ui.common.livedata.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class PlayerViewModel(
    private val videoUrl: String,
    private val getVideoPageUseCase: GetVideoPageUseCase
) : BaseViewModel<String>() {

    private val disposables = CompositeDisposable()
    private val _event = SingleLiveEvent<Event>()

    val event: LiveData<Event> get() = _event

    init {
        getVideoUrl()
    }

    fun onRetryClicked() {
        getVideoUrl()
    }

    private fun getVideoUrl() {
        disposables.add(getVideoPageUseCase(videoUrl)
            .map { videoPage ->
                // TODO if there are several streams info it could be useful to seek to the next
                videoPage.streamsInfo.first().url
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::postUiData, ::onErrorReceived)
        )
    }

    private fun onErrorReceived(e: Throwable) {
        _event.value = when {
            e.isNetworkRelated() -> Event.Error.NetworkError
            else -> Event.Error.GenericError(e.message)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    sealed class Event {
        sealed class Error : Event() {
            object NetworkError : Error()
            class GenericError(val message: String?) : Error()
        }
        // there could be other non error events also
    }
}
package com.canal.android.test.repository.api.model

data class OnClickApi(
    val displayTemplate: String?,
    val displayName: String?,
    val URLPage: String,
    val URLMedias: String?
)
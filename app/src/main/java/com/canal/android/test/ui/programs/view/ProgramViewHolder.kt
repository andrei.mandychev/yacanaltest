package com.canal.android.test.ui.programs.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.canal.android.test.ui.programs.model.ProgramUi
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.layout_program.view.*

class ProgramViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(programUi: ProgramUi) {
        with (containerView) {

            program_title.text = programUi.title

            program_subtitle.text = programUi.subtitle

            Glide.with(this)
                .load(programUi.urlImage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(program_image)

            programUi.urlLogoChannel?.let {
                Glide.with(this)
                    .load(programUi.urlImage)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(program_channel_image)
            }
        }
    }
}
package com.canal.android.test.ui.programs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.canal.android.test.R
import com.canal.android.test.domain.model.NavigateTo
import com.canal.android.test.ui.programs.ProgramsViewModel.Event
import com.canal.android.test.ui.programs.model.ProgramUi
import com.canal.android.test.ui.programs.view.adapter.ProgramsAdapter
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_programs.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProgramsFragment : Fragment() {

    private val viewModel: ProgramsViewModel by viewModel()
    private val recyclerAdapter = ProgramsAdapter(::onProgramClicked)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_programs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        programs_recycler.apply {
            layoutManager = LinearLayoutManager(view.context)
            adapter = recyclerAdapter
        }

        viewModel.uiData.observe(viewLifecycleOwner, Observer { pageProgramsUi ->
            recyclerAdapter.submitList(pageProgramsUi.programs)
        })
        viewModel.event.observe(viewLifecycleOwner, Observer { onEventReceived(it) })
    }

    private fun onProgramClicked(programUi: ProgramUi) {
        val direction = when (programUi.navigateTo) {
            is NavigateTo.QuickTime ->
                ProgramsFragmentDirections.actionFragmentProgramsToPlayerFragment(programUi.navigateTo.urlMedias)
            is NavigateTo.DetailPage ->
                ProgramsFragmentDirections.actionFragmentProgramsToDetailsFragment(programUi)
        }
        findNavController().navigate(direction)
    }

    private fun onEventReceived(event: Event) {
        when (event) {
            is Event.Error -> showErrorEvent(event)
        }
    }

    private fun showErrorEvent(errorEvent: Event.Error) {
        val errorMessage = when (errorEvent) {
            is Event.Error.GenericError -> errorEvent.message
            is Event.Error.NetworkError -> getString(R.string.error_network)
        } ?: getString(R.string.error_generic)

        view?.let {
            Snackbar.make(it, errorMessage, Snackbar.LENGTH_LONG)
                .setAction(R.string.retry) { viewModel.onRetryClicked() }
                .show()
        }

    }
}
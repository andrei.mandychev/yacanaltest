package com.canal.android.test.repository

import com.canal.android.test.repository.mapper.ProgramMapper
import com.canal.android.test.domain.Repository
import com.canal.android.test.domain.model.DetailsPage
import com.canal.android.test.domain.model.Program
import com.canal.android.test.domain.model.VideoPage
import com.canal.android.test.repository.mapper.DetailsPageMapper
import com.canal.android.test.repository.mapper.PageMapper
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class RepositoryImpl(
    private val apiDataSource: ApiDataSource,
    private val programMapper: ProgramMapper,
    private val pageMapper: PageMapper,
    private val detailsPageMapper: DetailsPageMapper
) : Repository {

    override fun getPrograms(url: String): Single<List<Program>> {
        return apiDataSource.getPrograms(url)
            .map { programsApi ->
                programsApi.map { programApi ->
                    programMapper.toDomain(programApi)
                }
            }.subscribeOn(Schedulers.io())
    }

    override fun getVideoPage(url: String): Single<VideoPage> =
        apiDataSource.getVideoPage(url).map { pageMapper.toDomain(it) }
            .subscribeOn(Schedulers.io())

    override fun getDetailsPage(url: String): Single<DetailsPage> =
        apiDataSource.getDetailsPage(url).map { detailsPageMapper.toDomain(it) }
            .subscribeOn(Schedulers.io())
}
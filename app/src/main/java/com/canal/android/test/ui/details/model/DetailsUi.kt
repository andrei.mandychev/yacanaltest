package com.canal.android.test.ui.details.model

data class DetailsUi(
    val id: String,
    val title: String,
    val subtitle: String,
    val urlImage: String,
    val summary: String,
    val editorialTitle: String,
    val urlMedias: String
)
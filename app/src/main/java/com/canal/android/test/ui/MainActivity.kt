package com.canal.android.test.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.canal.android.test.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}

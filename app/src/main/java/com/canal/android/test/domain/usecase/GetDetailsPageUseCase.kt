package com.canal.android.test.domain.usecase

import com.canal.android.test.domain.Repository
import com.canal.android.test.domain.model.DetailsPage
import io.reactivex.Single

class GetDetailsPageUseCase(
    private val repository: Repository
) {

    operator fun invoke(url: String): Single<DetailsPage> = repository.getDetailsPage(url)
}
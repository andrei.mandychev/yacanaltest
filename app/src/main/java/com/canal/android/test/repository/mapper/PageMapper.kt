package com.canal.android.test.repository.mapper

import com.canal.android.test.domain.model.VideoPage
import com.canal.android.test.repository.mapper.PageMapper.ProtocolException.*
import com.canal.android.test.repository.api.model.VideoPageApi
import java.lang.Exception

class PageMapper : BaseDomainMapper<VideoPageApi, VideoPage>() {

    override fun toDomain(api: VideoPageApi): VideoPage {
        val streamsInfo = api.detail?.let { detail ->
            detail.informations?.let { info ->
                info.videoUrls?.map {
                    VideoPage.StreamInfo(
                        it.url ?: throw NullUrlException,
                        it.encryption ?: throw NullEncryptionException
                    )
                }
            } ?: throw NullInformationException
        } ?: throw NullDetailException

        return if (streamsInfo.isNotEmpty()) VideoPage(streamsInfo) else throw EmptyDetailException
    }

    sealed class ProtocolException : Exception() {
        object NullDetailException : ProtocolException()
        object NullInformationException : ProtocolException()
        object NullUrlException : ProtocolException()
        object NullEncryptionException : ProtocolException()
        object EmptyDetailException : ProtocolException()
    }
}
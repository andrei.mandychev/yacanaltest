package com.canal.android.test.domain.model

data class DetailsPage(
    val id: String,
    val title: String,
    val subtitle: String,
    val urlImage: String,
    val summary: String,
    val editorialTitle: String,
    val urlMedias: String
)
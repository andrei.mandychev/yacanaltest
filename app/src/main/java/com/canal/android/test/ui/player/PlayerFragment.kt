package com.canal.android.test.ui.player

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.canal.android.test.R
import com.canal.android.test.ui.player.PlayerViewModel.Event
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.util.Util
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_player.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import kotlin.math.max

class PlayerFragment : Fragment() {

    private val playerHelper: PlayerHelper by inject()

    private val viewModel: PlayerViewModel by viewModel {
        parametersOf(PlayerFragmentArgs.fromBundle(requireNotNull(arguments)).videoUrl)
    }

    private var player: SimpleExoPlayer? = null
    private var mediaSource: MediaSource? = null

    private var startAutoPlay = true
    private var startWindow: Int = 0
    private var startPosition: Long = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

        savedInstanceState?.apply {
            startAutoPlay = getBoolean(KEY_AUTO_PLAY)
            startWindow = getInt(KEY_WINDOW)
            startPosition = getLong(KEY_POSITION)
        }
        viewModel.uiData.observe(viewLifecycleOwner, Observer { onVideoUrlReceived(it) })
        viewModel.event.observe(viewLifecycleOwner, Observer { onEventReceived(it) })
    }

    override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initializePlayer()
            if (playerView != null) {
                playerView.onResume()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (Util.SDK_INT <= 23 || player == null) {
            initializePlayer()
            if (playerView != null) {
                playerView.onResume()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(KEY_AUTO_PLAY, startAutoPlay)
        outState.putInt(KEY_WINDOW, startWindow)
        outState.putLong(KEY_POSITION, startPosition)
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            if (playerView != null) {
                playerView.onPause()
            }
            releasePlayer()
        }
    }

    override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            if (playerView != null) {
                playerView.onPause()
            }
            releasePlayer()
        }
    }

    override fun onDetach() {
        super.onDetach()
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    private fun initializePlayer() {
        val exoPlayer = player ?: run {
            SimpleExoPlayer.Builder(requireContext(), playerHelper.rendersFactory).build().apply {
                playerView.player = this
                player = this
                playWhenReady = startAutoPlay
            }
        }

        mediaSource?.let { exoPlayer.launch(it) }
    }

    private fun releasePlayer() {
        player?.apply {
            startAutoPlay = playWhenReady
            startWindow = currentWindowIndex
            startPosition = max(0, contentPosition)
            release()
        }
        player = null
    }

    private fun onVideoUrlReceived(videoUrl: String) {
        val source = playerHelper.createMediaSource(videoUrl)
        mediaSource = source
        // Start the player if it's already initialized, otherwise it wil be started from onResume
        // since we are not sure about the moment when videoUrl is received.
        // Normally it should be after onResume due to IO/network calls.
        player?.launch(source)
    }

    private fun onEventReceived(event: Event) {
        when (event) {
            is Event.Error -> showErrorEvent(event)
        }
    }

    private fun showErrorEvent(errorEvent: Event.Error) {
        val errorMessage = when (errorEvent) {
            is Event.Error.GenericError -> errorEvent.message
            is Event.Error.NetworkError -> getString(R.string.error_network)
        } ?: getString(R.string.error_generic)

        Snackbar.make(player_content, errorMessage, Snackbar.LENGTH_LONG)
            .setAnchorView(guideline_error)
            .setAction(R.string.retry) { viewModel.onRetryClicked() }
            .show()
    }

    private fun SimpleExoPlayer.launch(source: MediaSource) {
        val haveStartPosition = startWindow != C.INDEX_UNSET
        if (haveStartPosition) {
            seekTo(startWindow, startPosition)
        }
        prepare(source, !haveStartPosition, false)
    }

    private companion object {
        const val KEY_WINDOW = "window"
        const val KEY_POSITION = "position"
        const val KEY_AUTO_PLAY = "auto_play"
    }
}
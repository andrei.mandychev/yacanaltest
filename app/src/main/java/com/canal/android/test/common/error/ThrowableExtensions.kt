package com.canal.android.test.common.error

import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

fun Throwable.isNetworkRelated(): Boolean = this is ConnectException ||
        this is UnknownHostException ||
        this is SocketTimeoutException ||
        this is TimeoutException
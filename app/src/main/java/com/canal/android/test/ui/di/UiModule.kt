package com.canal.android.test.ui.di

import com.canal.android.test.ui.details.DetailsViewModel
import com.canal.android.test.ui.details.mapper.DetailsUiMapper
import com.canal.android.test.ui.player.PlayerHelper
import com.canal.android.test.ui.player.PlayerViewModel
import com.canal.android.test.ui.programs.ProgramsViewModel
import com.canal.android.test.ui.programs.mapper.ProgramsUiMapper
import com.canal.android.test.ui.programs.model.ProgramUi
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val uiProgramsViewModelModule = module {
    single {
        ProgramsUiMapper()
    }
    viewModel {
        ProgramsViewModel(
            getProgramsUseCase = get(),
            programsUiMapper = get()
        )
    }
}

val uiDetailsModule = module {
    single {
        DetailsUiMapper()
    }
    viewModel { (programUi: ProgramUi) ->
        DetailsViewModel(
            programUi,
            getDetailsPageUseCase = get(),
            mapper = get()
        )
    }
}

val uiPlayerModule = module {
    single {
        PlayerHelper(androidContext())
    }
    viewModel { (videoUrl: String) ->
        PlayerViewModel(
            videoUrl,
            getVideoPageUseCase = get()
        )
    }
}

val koinUiModules = listOf(
    uiProgramsViewModelModule,
    uiDetailsModule,
    uiPlayerModule
)

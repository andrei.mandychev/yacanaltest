package com.canal.android.test.domain.model

data class VideoPage(
    val streamsInfo: List<StreamInfo>
) {

    data class StreamInfo(
        val url: String,
        val encryption: String
    )
}
package com.canal.android.test.ui.details.mapper

import com.canal.android.test.domain.model.DetailsPage
import com.canal.android.test.ui.common.BaseUiMapper
import com.canal.android.test.ui.details.model.DetailsUi

class DetailsUiMapper : BaseUiMapper<DetailsPage, DetailsUi>() {

    override fun toUi(domain: DetailsPage): DetailsUi = domain.let {
        DetailsUi(
            it.id,
            it.title,
            it.subtitle,
            it.urlImage,
            it.summary,
            it.editorialTitle,
            it.urlMedias
        )
    }
}
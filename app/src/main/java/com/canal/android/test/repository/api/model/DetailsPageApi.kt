package com.canal.android.test.repository.api.model

import com.squareup.moshi.Json

data class DetailsPageApi(
    val detail: DetailApi?
) {

    data class DetailApi(
        val informations: InformationsApi?
    ) {

        data class InformationsApi(
            @Json(name = "contentID") val id: String?,
            val title: String?,
            val subtitle: String?,
            val editorialTitle: String?,
            val summary: String?,
            @Json(name = "URLImage") val urlImage: String?,
            @Json(name = "URLMedias") val urlMedia: String?
        )
    }

}
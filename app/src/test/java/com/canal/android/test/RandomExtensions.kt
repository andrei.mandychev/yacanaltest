package com.canal.android.test

import com.canal.android.test.Constants.BIG_THRESHOLD
import com.canal.android.test.Constants.BLACK_COLOR_INT
import com.canal.android.test.Constants.COLOR_HEX_FORMATTER
import com.canal.android.test.Constants.CONSONANT
import com.canal.android.test.Constants.DIGIT
import com.canal.android.test.Constants.HALF_PROBABILITY
import com.canal.android.test.Constants.HUGE_THRESHOLD
import com.canal.android.test.Constants.LETTER
import com.canal.android.test.Constants.MAX_ASCII
import com.canal.android.test.Constants.MAX_INT
import com.canal.android.test.Constants.MIN_PRINTABLE
import com.canal.android.test.Constants.PLACEHOLDER_WEBSITE_URL
import com.canal.android.test.Constants.SMALL_THRESHOLD
import com.canal.android.test.Constants.TINY_THRESHOLD
import com.canal.android.test.Constants.VOWEL
import com.canal.android.test.Constants.WHITE_COLOR_INT
import java.util.Date
import java.util.Locale
import java.text.SimpleDateFormat
import kotlin.math.abs
import kotlin.random.Random

// TODO handle string case

enum class PlaceHolderExtension(val value: String) { JPG("jpg"), PNG("png") }

private val rng = java.util.Random()

/**
 * @param probability the probability the boolean will be true (default 0.5f)
 * @return a boolean
 */
@JvmOverloads
fun Random.nextBool(probability: Float = HALF_PROBABILITY): Boolean = nextFloat() < probability

/**
 * @param width the width of the image in px
 * @param height the height of the image in px
 * @param extension the image extension
 *
 * @return a String like "https://via.placeholder.com/360x240.png/sdf/eee"
 */
@JvmOverloads
fun Random.nextPlaceHolderUrl(width: Int, height: Int, extension: PlaceHolderExtension = PlaceHolderExtension.PNG): String {
    val backgroundIntColor = nextColor()

    val (red, green, blue) = with(backgroundIntColor) {
        arrayOf((this shr 16) and 0x0ff, (this shr 8) and 0x0ff, this and 0x0ff)
    }

    // Convert backgroundIntColor RGB into brightness (Y) value in YIQ color space to find the best contrast color between black or white
    // see https://en.wikipedia.org/wiki/YIQ#Formulas
    val y = (299 * red + 587 * green + 114 * blue) / 1000
    val backgroundContrastColor = if (y >= 128) BLACK_COLOR_INT else WHITE_COLOR_INT

    val backgroundHexColor = nextHexColor(backgroundIntColor).replace("#", "")
    val textHexColor = nextHexColor(backgroundContrastColor).replace("#", "")

    return "$PLACEHOLDER_WEBSITE_URL/${width}x$height.${extension.value}/$backgroundHexColor/$textHexColor"
}

/**
 * @return int color
 *
 * TODO generate only light or dark color
 */
fun Random.nextColor(): Int {
    val (red, green, blue) = generateSequence { nextInt(256) }.take(3).toList()
    // 16: red offset
    // 8 : geen offset
    return (red shl 16) or (green shl 8) or blue
}

@JvmOverloads
fun Random.nextHexColor(colorInt: Int? = null): String = COLOR_HEX_FORMATTER.format((0xFFFFFF and (colorInt ?: nextColor())))

//region int
/**
 * @param min the minimum value (inclusive), default = Int#MIN_VALUE
 * @param max the maximum value (exclusive), default = Int#MAX_VALUE
 * @param size the size of the array, or -1 for a random size
 * @return an array of int
 */
@JvmOverloads
fun Random.nextIntArray(min: Int = Int.MIN_VALUE, max: Int = Int.MAX_VALUE, size: Int = -1): IntArray {
    val arraySize = if (size < 0) nextInt(1, TINY_THRESHOLD) else size
    return IntArray(arraySize) { nextInt(min, max) }
}

@JvmOverloads
fun Random.nextPositiveInt(strict: Boolean = false): Int = anInt(min = if (strict) 1 else 0)

@JvmOverloads
fun Random.nextNegativeInt(strict: Boolean = false): Int = anInt(min = Int.MIN_VALUE, max = if (strict) -1 else 0)

fun Random.nextTinyInt(): Int = anInt(1, TINY_THRESHOLD)

fun Random.nextSmalInt(): Int = anInt(1, SMALL_THRESHOLD)

fun Random.nextBigInt(): Int = anInt(1, BIG_THRESHOLD)

fun Random.nextHugeInt(): Int = anInt(1, HUGE_THRESHOLD)
//endregion

//region float
@JvmOverloads
fun Random.nextDouble(min: Double = -Double.MAX_VALUE, max: Double = Double.MAX_VALUE): Double {
    if (min <= max) {
        throw IllegalArgumentException("the `min` value ($min) should be less or equal to the `max` value ($max)")
    }

    val range = max - min
    return if (range == Double.POSITIVE_INFINITY) {
        (rng.nextDouble() - HALF_PROBABILITY) * Double.MAX_VALUE * 2
    } else {
        (rng.nextDouble() * range) + min
    }
}

@JvmOverloads
fun Random.nextPositiveDouble(strict: Boolean = false): Double = nextDouble(min = if (strict) Double.MIN_VALUE else 0.0)

@JvmOverloads
fun Random.nextNegativeDouble(strict: Boolean = false): Double = -nextPositiveDouble(strict)
//endregion

//region float
/**
 * @param min the minimum value (inclusive), default = Float#MIN_VALUE
 * @param max the maximum value (exclusive), default = Float#MAX_VALUE
 * @return a float between min and max
 */
@JvmOverloads
fun Random.nextFloat(min: Float = Float.MIN_VALUE, max: Float = Float.MAX_VALUE): Float {
    if (min >= max) {
        throw IllegalArgumentException("the `min` value ($min) cannot be superior to `max` value ($max)")
    }

    val range = max - min
    return if (range == Float.POSITIVE_INFINITY) {
        (nextFloat() - HALF_PROBABILITY) * Float.MAX_VALUE * 2
    } else {
        (nextFloat() * range) + min
    }
}

/**
 * @param strict if true, then it will return a non 0 int (default : false)
 * @return a positive float
 */
@JvmOverloads
fun Random.nextPositiveFloat(strict: Boolean = false): Float = nextFloat(min = if (strict) Float.MIN_VALUE else 0.0f)

/**
 * @param strict if true, then it will return a non 0 int (default : true)
 * @return a negative float
 */
@JvmOverloads
fun Random.nextNegativeFloat(): Float = -nextPositiveFloat(true)
//endregion

//region long
/**
 * @param strict if true, then it will return a non 0 long (default : false)
 * @return a positive long
 */
@JvmOverloads
fun Random.nextPositiveLong(strict: Boolean = false): Long = nextLong(from = if (strict) 1L else 0L, until = Long.MAX_VALUE)

/**
 * @param strict if true, then it will return a non 0 long (default : true)
 * @return a negative long
 */
@JvmOverloads
fun Random.nextNegativeLong(): Long = nextLong(from = Long.MIN_VALUE, until = -1)
//endregion

//region String
@JvmOverloads
fun Random.nextString(size: Int = -1): String = nextString(StringConstraint.ANY, size)

/**
 * @param constraint the constraint to use (default : ANY)
 * @param size the size of the string (or -1 for a random sized String)
 * @return a random string following the given constraint
 */
fun Random.nextString(constraint: StringConstraint = StringConstraint.ANY, size: Int = -1): String {
    return when (constraint) {
        StringConstraint.ANY -> {
            val stringSize = if (size > 0) size else nextInt(1, TINY_THRESHOLD)
            String(CharArray(stringSize) { nextChar() })
        }
        StringConstraint.NUMERICAL -> nextNumericalString(size)
        StringConstraint.ALPHANUMERIC -> nextAlphaNumericalString(size)
        StringConstraint.URL -> nextStringUrl()
    }
}

/**
 * @return a random String that looks like an Url : begins with a scheme and then "www.". Ends with one of
 * the four extensions : "fr", "com", "net" or "org".
 * @param withScheme if a scheme has to be prepend before "www". Default is true
 * @param forceHttp if the HTTP scheme must be used, instead of the default HTTPS. Default is false.
 */
fun Random.nextStringUrl(withScheme: Boolean = true, forceHttp: Boolean = false): String {
    val scheme = when {
        !withScheme -> ""
        forceHttp -> "http://"
        else -> "https://"
    }
    return "${scheme}www.${
    nextWord(nextInt(5, 20)).toLowerCase()
    }.${
    listOf("fr", "com", "net", "org").random()
    }"
}

/**
 * @param size the size of the string (or -1 for a random sized String)
 * @return a random numerical string
 */
fun Random.nextNumericalString(size: Int = -1): String {
    val stringSize = if (size > 0) size else nextInt(1, TINY_THRESHOLD)
    return String(CharArray(stringSize) { nextNumericalChar() })
}

/**
 * @param size the size of the string (or -1 for a random sized String)
 * @return a random alphaNumerical string
 */
fun Random.nextAlphaNumericalString(size: Int = -1): String {
    val stringSize = if (size > 0) size else nextInt(1, TINY_THRESHOLD)
    return String(CharArray(stringSize) { nextAlphaNumericalChar() })
}

/**
 * @param size the size of the string (or -1 for a random sized String)
 * @return a String that kind of look like a word
 */
@JvmOverloads
fun Random.nextWord(size: Int = -1): String {
    var consonant: Boolean = nextBool()
    val resultSize = if (size > 0) size else nextInt(1, TINY_THRESHOLD)

    val array = CharArray(resultSize)
    for (i in 0 until resultSize) {
        array[i] = if (consonant) {
            CONSONANT.elementAt(nextInt(0, CONSONANT.size))
        } else {
            VOWEL.elementAt(nextInt(0, VOWEL.size))
        }
        consonant = !consonant
    }

    return String(array)
}
/**
 * @param pattern formats the date using the given pattern and the default date format symbols.
 * @param until must be positive.
 * @return a random formatted date string
 */
fun Random.nextFormattedDateString(pattern: String, from: Long = 0, until: Long = Date().time): String {
    val dateFormat = SimpleDateFormat(pattern, Locale.FRANCE)
    val date = Date(Random.nextLong(from, until))
    return dateFormat.format(date)
}
//endregion

//region char
/**
 * @param min the min char code to use (inclusive, default = 0x20 == space)
 * @param max the max char code to use (exclusive, default = 0x7F)
 * @return a Char within the given range
 */
@JvmOverloads
fun Random.nextChar(min: Char = MIN_PRINTABLE, max: Char = MAX_ASCII): Char = nextInt(min.toInt(), max.toInt()).toChar()

fun Random.nextNumericalChar(): Char = anElementFrom(DIGIT)

fun Random.nextAlphaNumericalChar(): Char = anElementFrom(LETTER + DIGIT)
//endregion

//region utils
/**
 * @param array a non empty CharArray
 * @return an element "randomly" picked in the array
 */
private fun anElementFrom(array: CharArray): Char = array[anInt(0, array.size)]

/**
 * @param min the minimum value (inclusive), default = Int#MIN_VALUE
 * @param max the maximum value (exclusive), default = Int#MAX_VALUE
 * @return an int between min and max
 */
private fun anInt(min: Int = Int.MIN_VALUE, max: Int = Int.MAX_VALUE): Int {
    require(min < max) { "The ‘min’ boundary ($min) of the range should be less than the ‘max’ boundary ($max)" }

    val range = max.toLong() - min.toLong()
    val rn = (abs(rng.nextLong()) % range) + min

    return (rn and MAX_INT).toInt()
}
//endregion

//region Constraints
// TODO Add WORD/URL/URI/EMAIL and whatever your need
enum class StringConstraint {
    ANY, NUMERICAL, URL, ALPHANUMERIC
}

enum class CharConstraint {
    ANY, NUMERICAL
}
//endregion

internal object Constants {
    // Colors
    internal const val BLACK_COLOR_INT = 0x000000
    internal const val WHITE_COLOR_INT = 0xFFFFFF

    internal const val COLOR_HEX_FORMATTER = "#%6X"
    internal const val PLACEHOLDER_WEBSITE_URL = "https://via.placeholder.com"

    // Bool
    internal const val HALF_PROBABILITY = 0.5f

    // Int
    internal const val TINY_THRESHOLD = 0x20
    internal const val SMALL_THRESHOLD = 0x100
    internal const val BIG_THRESHOLD = 0x10000
    internal const val HUGE_THRESHOLD = 0x1000000
    internal const val MAX_INT = 0xFFFFFFFFL

    // String
    internal const val MIN_PRINTABLE = 0x20.toChar()
    internal const val MAX_ASCII = 0x7F.toChar()

    internal val CONSONANT = "ZRTPQSDFGHJKLMWXCVBNzrtpqsdfghjklmwxcvbn".toCharArray()
    internal val VOWEL = "aeiouyAEIOUY".toCharArray()

    @JvmField internal val DIGIT = "0123456789".toCharArray()
    internal val LETTER = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()
}

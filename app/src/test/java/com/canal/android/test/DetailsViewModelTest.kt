package com.canal.android.test

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.canal.android.test.domain.model.DetailsPage
import com.canal.android.test.domain.model.NavigateTo
import com.canal.android.test.domain.usecase.GetDetailsPageUseCase
import com.canal.android.test.ui.details.DetailsViewModel
import com.canal.android.test.ui.details.mapper.DetailsUiMapper
import com.canal.android.test.ui.programs.model.ProgramUi
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject
import org.robolectric.RobolectricTestRunner
import kotlin.random.Random

@RunWith(RobolectricTestRunner::class) // For RxJava using android.os.Looper
class DetailsViewModelTest : KoinTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(module {
            single {
                DetailsUiMapper()
            }
        })
    }

    private val getDetailsPageUseCase = mockk<GetDetailsPageUseCase>()
    private val detailsUiMapper: DetailsUiMapper by inject()

    @Test
    fun `test normal scenario`() {
        // Given
        val id = Random.nextString()
        val title = Random.nextString()
        val subtitle = Random.nextString()
        val urlImage = Random.nextString()
        val urlLogo = Random.nextString()
        val urlPage = Random.nextString()
        val navigateTo = NavigateTo.DetailPage(urlPage)
        val programUi = ProgramUi(id, title, subtitle, urlImage, urlLogo, navigateTo)

        val summary = Random.nextString()
        val editorialTitle = Random.nextString()
        val urlMedias = Random.nextString()
        val detailsPage = DetailsPage(id, title, subtitle, urlImage, summary, editorialTitle, urlMedias)
        every { getDetailsPageUseCase.invoke(any()) } returns Single.just(detailsPage)

        // When
        val detailsUi = DetailsViewModel(programUi, getDetailsPageUseCase, detailsUiMapper).uiData.test().value()

        // Then
        verify(exactly = 1) { getDetailsPageUseCase.invoke(urlPage) }
        Assert.assertEquals(id, detailsUi.id)
        Assert.assertEquals(title, detailsUi.title)
        Assert.assertEquals(subtitle, detailsUi.subtitle)
        Assert.assertEquals(urlImage, detailsUi.urlImage)
        Assert.assertEquals(summary, detailsUi.summary)
        Assert.assertEquals(editorialTitle, detailsUi.editorialTitle)
        Assert.assertEquals(urlMedias, detailsUi.urlMedias)
    }

    @Test
    fun `test on play click`() {
        // Given
        val id = Random.nextString()
        val title = Random.nextString()
        val subtitle = Random.nextString()
        val urlImage = Random.nextString()
        val urlLogo = Random.nextString()
        val urlPage = Random.nextString()
        val navigateTo = NavigateTo.DetailPage(urlPage)
        val programUi = ProgramUi(id, title, subtitle, urlImage, urlLogo, navigateTo)

        val summary = Random.nextString()
        val editorialTitle = Random.nextString()
        val urlMedias = Random.nextString()
        val detailsPage = DetailsPage(id, title, subtitle, urlImage, summary, editorialTitle, urlMedias)
        every { getDetailsPageUseCase.invoke(any()) } returns Single.just(detailsPage)

        val viewModel = DetailsViewModel(programUi, getDetailsPageUseCase, detailsUiMapper)
        val observer = viewModel.event.test()

        // When
        viewModel.onPlayClicked()

        // Then
        val events = observer.valueHistory()
        Assert.assertEquals(1, events.size)
        Assert.assertTrue(events.first() is DetailsViewModel.Event.OnPlayClicked)
        val onPlayClickedEvent = events.first() as DetailsViewModel.Event.OnPlayClicked
        Assert.assertEquals(urlMedias, onPlayClickedEvent.videoUrl)
    }

}